# Cewarib


## Overview

**Cewarib** (pronounced *juh-wah-ribb*, which means "socks" in my native tongue)
is a simple SOCKS5 server implementation, written as a standalone Python script.

### Features

- Procedural codebase: no classes, except for *exceptions* (pun unintended)
- Threaded: one thread per incoming connection
- Server-side domain name resolution support
- Full IPv6 support with fallback to IPv4
- Username/password authentication support

TODO:

- Enforcing ruleset to mitigate security disks (i.e. firewalling)
- UDP ASSOCIATE requests support
- BIND requests support
- GSSAPI authentication support
- More flexible logging mechanism


## Installation

**Cewarib** was tested on *Debian GNU/Linux sid*, but it may work on other
platforms with little to no tweaking.

The single dependency of **Cewarib** is the Python interpreter itself (and its
standard library, of course).

```sh
# Install the script
sudo install cewarib.py /usr/local/bin/cewarib

# [Optional] Install a global configuration file
sudo install -m644 cewarib.ini /etc/

# [Optional] Install a user-specific configuration file
install -m644 cewarib.ini ~/.cewarib.ini

# [Optional] Install the systemd service file (if you use systemd)
sudo install -m644 cewarib.service /etc/systemd/system/
```

**Cewarib** tries to read configuration from these files (with the late
overriding the early):

- `/etc/cewarib.ini`
- `~/.cewarib.ini`


## Usage

### As a normal user

You can just run `cewarib` (or `cewarib &` to daemonize it).  **Cewarib**
completely ignores command-line arguments.

To reload configuration at runtime, send a SIGHUP to the **Cewarib** process.

```sh
kill -HUP $(cat /run/user/$USER/cewarib.pid)
```

### As a systemd service (recommended)

You can use the provided *systemd* service to start/stop **Cewarib**, and make
it start automatically at boot.

```sh
# Enable and start the Cewarib service
sudo systemctl enable --now cewarib.service

# Reload configuration at runtime
sudo systemctl reload cewarib.service
```


## License

> Copyright (C) 2022 Karam Assany (karam.assany@disroot.org)
>
> Cewarib is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> Cewarib is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with Cewarib. If not, see <https://www.gnu.org/licenses/>
