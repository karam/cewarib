#!/usr/bin/env python3
#
# Cewarib v1.0: a simple SOCKS5 server implementation in Python 3
#
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# Cewarib is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cewarib is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Cewarib.  If not, see <https://www.gnu.org/licenses/>.
#


import sys, os, signal, configparser, hashlib, socket, errno, select, \
        threading, traceback



### CONSONANTS


## Basic constants

# "05" Version of the SOCKS protocol
VER = b"\x05"

# "00" Reserved null-byte
RSV = b"\x00"


## Subnegotation constants

# "01" Version of the username/password subnegotation
S_VER  = b"\x01"

# "00" Succeed of authentication
S_PASS = b"\x00"

# "01" Failure of authentication
S_FAIL = b"\x01"


## METHOD constants

# "00" NO AUTHENTICATION REQUIRED
M_NOAUTH     = b"\x00"

# "01" GSSAPI
M_GSSAPI     = b"\x01"

# "02" USERNAME/PASSWORD
M_USERPASS   = b"\x02"

# "FF" NO ACCEPTABLE METHODS (as a response)
M_UNACCEPTED = b"\xff"


## CMD constants

# "01" CONNECT
C_CONNECT = b"\x01"

# "02" BIND
C_BIND    = b"\x02"

# "03" UDP ASSOCIATE
C_UDP     = b"\x03"


## ATYP constants

# "01" IP V4 address
A_IPV4 = b"\x01"

# "03" DOMAINNAME
A_NAME = b"\x03"

# "04" IP V6 address
A_IPV6 = b"\x04"


## REP constants

# "00" succeeded
R_SUCCEED = b"\x00"

# "01" general SOCKS server failure
R_FAILURE = b"\x01"

# "02" conenction not allowed by ruleset
R_DENIED  = b"\x02"

# "03" Network unreachable
R_UNNET   = b"\x03"

# "04" Host unreachable
R_UNHOST  = b"\x04"

# "05" Connection refused
R_REFUSED = b"\x05"

# "06" TTL expired
R_EXPIRED = b"\x06"

# "07" Command not supported
R_INVCMD  = b"\x07"

# "08" Address type not supported
R_INVADR  = b"\x08"



### GLOBAL VARIABLES

## Internal global variables

thread_id = 0


## Configuration variables (assigned at by load_config)

enable_log         = None
server_port        = None

buffer_size        = None
socket_timeout     = None
max_connections    = None
force_ipv4         = None

auth_mandated      = None
auth_username      = None
auth_password_hash = None



## CLASSES (Exceptions)

class ServerFailure       (Exception): pass
class AbortedConnection   (Exception): pass
class FailedAuthentication(Exception): pass
class FailedRequest       (Exception): pass
class FallbackToIPv4      (Exception): pass
class QuitProxyLoop       (Exception): pass



## FUNCTIONS

def receive_sigterm(sig, frame):

    raise KeyboardInterrupt


def load_config(sig = None, frame = None):

    log("Reloading configuration")

    # List all configuration variables
    global thread_id, buffer_size, socket_timeout, max_connections, \
            enable_log, server_port, auth_mandated, auth_username, \
            auth_password_hash, force_ipv4

    conf = configparser.ConfigParser()
    conf.read(("/etc/cewarib.ini", os.path.expanduser("~/.cewarib.ini")))
        # order is important

    enable_log = conf.getboolean("server", "enable_log", fallback = False)
    server_port = conf.getint("server", "inbound_port", fallback = 1080)

    buffer_size = conf.getint("socket", "buffer_size", fallback = 256) * 1024
        # buffer_size in kilobytes
    socket_timeout = conf.getint("socket", "timeout", fallback = 60)
    max_connections = conf.getint("socket", "max_connections", fallback = 128)
    force_ipv4 = conf.getboolean("socket", "force_ipv4", fallback = False)

    auth_mandated = conf.getboolean("auth", "mandated", fallback = False)
    auth_username = conf.get("auth", "username", fallback = "user")
    auth_password_hash = conf.get("auth", "password_hash", fallback =
            "d74ff0ee8da3b9806b18c877dbf29bbde50b5bd8e4dad7a3a725000feb82e8f1")
            # sha256 hash for "pass"


def log(message, report_traceback = False):

    if enable_log:
        
        if threading.current_thread() == threading.main_thread():
            thread = "MAIN"
        else:
            thread = "%04d" % thread_id

        print("[{}] {}".format(thread, message), file = sys.stderr)

    if report_traceback:
        traceback.print_exc()


def undergo_client_connection(client):

    dest = None

    try:

        try:
            log("Receiving identification packet from client")
            ident_packet = client.recv(buffer_size)

        except socket.error:
            log("Cannot receive the packet.  Will abort", True)
            raise AbortedConnection("No identification packet")

        log("Checking requested SOCKS version")
        if ident_packet[:1] != VER:
            raise AbortedConnection("Requested SOCKS version not supported")

        methods  = ident_packet[2:2 + ident_packet[1]]

        chosen_method = M_UNACCEPTED
        acceptable    = M_USERPASS if auth_mandated else M_NOAUTH
            # GSSAPI support to be implemented

        log("Checking if offered method are given")

        log("Iterating through offered methods for an acceptable method")
        for method in methods:
            meth = chr(method).encode()
            if meth == acceptable:
                log("Found an acceptable method")
                chosen_method = meth
                break

        try:
            log("Sending the accepted method reply to client")
            client.sendall(VER + chosen_method)

        except socket.error:
            log("Cannot send the reply.  Will abort", True)
            raise AbortedConnection("Cannot send the accepted method reply")

        if chosen_method == M_USERPASS:

            log("Username/password authentication method is used")

            try:
                log("Receiving the authentication packet from client")
                auth_packet = client.recv(buffer_size)

            except socket.error:
                log("Cannot receive the packet.  Will abort", True)
                raise AbortedConnection("No authentication packet")

            auth_status = S_FAIL

            try:

                log("Checking the subnegotation version")
                if auth_packet[:1] != S_VER:
                    raise FailedAuthentication("Invalid subnegotation version")

                username_len = auth_packet[1]
                password_len = auth_packet[1 + username_len]
                username = auth_packet[2:2 + username_len].decode()
                password_hash = hashlib.sha256(auth_packet[3 + username_len:3 +
                    username_len + password_len]).hexdigest()

                log("Checking username and password")
                if (username != auth_username) or (password_hash.lower() !=
                        auth_password_hash.lower()):
                    raise FailedAuthentication("Incorrect credentials")

                auth_status = S_PASS

            except FailedAuthentication as e:
                log("Authentication was unsuccessful: " + e.args[0])
                auth_status = S_FAIL

            finally:

                try:
                    log("Sending the auth reply to client")
                    client.sendall(S_VER + auth_status)
                    
                except socket.error:
                    log("Cannot send the reply.  Will abort", True)
                    raise AbortedConnection("Cannot confirm authentication")

            if auth_status != S_PASS:
                raise AbortedConnection("Failed to authenticate")

        elif chosen_method == M_NOAUTH:
            pass

        else:
            raise AbortedConnection("No method was accepted")

        try:
            log("Receiving request packet from the client")
            request = client.recv(buffer_size)
            
        except socket.error:
            log("Cannot receive the packet.  Will abort")
            raise AbortedConnection("Cannot receive the request packet")

        try:

            log("Checking if request was valid")
            if (request[:1] != VER) or (request[2:3] != RSV):
                raise FailedRequest("Headers are invalid", R_FAILURE)

            log("Checking requested command")

            cmd = request[1:2]

            if   cmd == C_CONNECT:
                pass
            elif cmd == C_BIND:
                raise FailedRequest("Command not supported", R_INVCMD)
                # To be implemented
            elif cmd == C_UDP:
                raise FailedRequest("Command not supported", R_INVCMD)
                # To be implemented
            else:
                raise FailedRequest("Command invalid", R_INVCMD)

            log("Checking and obtaining requested destination info")

            atyp = request[3:4]
                        
            if   atyp == A_IPV4:
                dest_host = socket.inet_ntop(socket.AF_INET, request[4:8])
                dest_port = ((request[8] & 0xFF) << 8) | (request[9] & 0xFF)
                
            elif atyp == A_NAME:
                domain_len = request[4]
                domain_end = 5 + domain_len
                dest_host = request[5:domain_end]
                dest_port = ((request[domain_end] & 0xFF) << 8) | \
                        (request[domain_end + 1] & 0xFF)

            elif atyp == A_IPV6:
                dest_host = socket.inet_ntop(socket.AF_INET6, request[4:20])
                dest_port = ((request[20] & 0xFF) << 8) | (request[21] & 0xFF)

            else:
                raise FailedRequest("Address type invalid", R_INVADR)

            try:

                if (not socket.has_ipv6) or force_ipv4 or (atyp == A_IPV4):
                    raise FallbackToIPv4  # force IPv4
                
                log("Trying to connect through IPv6")

                dest = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
                dest.settimeout(socket_timeout)
 
                try:
                    dest.connect((dest_host, dest_port))

                except OSError as e:
                    if e.errno == errno.ENETUNREACH:
                        raise FallbackToIPv4  # Try IPv4 if possible
                    elif e.errno == errno.EHOSTUNREACH:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("Host unreachable", R_UNHOST)
                    elif e.errno == errno.CONNREFUSED:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("Connection refused", R_REFUSED)
                    elif e.errno == errno.ETIME:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("TTL expired", R_EXPIRED)
                    else:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("General failure", R_FAILURE)
 
            except FallbackToIPv4:

                if atyp == A_IPV6:
                    raise FailedRequest("Address type not supported", R_INVADR)

                log("Using IPv4 now")

                dest = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                dest.settimeout(socket_timeout)
                
                try:
                    dest.connect((dest_host, dest_port))

                except OSError as e:
                    if e.errno == errno.ENETUNREACH:
                        raise FailedRequest("Network unreachable", R_UNNET)
                    elif e.errno == errno.EHOSTUNREACH:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("Host unreachable", R_UNHOST)
                    elif e.errno == errno.CONNREFUSED:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("Connection refused", R_REFUSED)
                    elif e.errno == errno.ETIME:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("TTL expired", R_EXPIRED)
                    else:
                        log("Cannot connect to destination", True)
                        raise FailedRequest("General failure", R_FAILURE)

            rep = R_SUCCEED

            rep_atyp = A_IPV4 if dest.family == socket.AF_INET else A_IPV6

            bind_addr = socket.inet_pton(dest.family, dest.getsockname()[0])
            bind_port = dest.getsockname()[1].to_bytes(2, 'big')

        except FailedRequest as e:
            log("Failed to perform request: " + e.args[0])

            rep = e.args[1]

            # dumb reply details
            rep_atyp = A_IPV6
            bind_addr = RSV * 16
            bind_port = RSV * 2

        try:
            log("Sending response reply to the client")
            client.sendall(VER + rep + RSV + rep_atyp + bind_addr + bind_port)

        except socket.error:
            log("Cannot send reply", True)
            raise AbortedConnection("Cannot send response reply")

        if rep != R_SUCCEED:
            raise AbortedConnection("Failed request")

        log("Starting the proxy loop")
        
        try:

            while True:

                try:
                    log("Selecting readable sockets of client and destination")
                    readable = select.select([client, dest], [], [], 0.01)[0]

                except select.error:
                    log("Cannot select reable sockets", True)
                    raise QuitProxyLoop("Selection error")

                if not readable:
                    log("No readable sockets at the moment.  Trying again")
                    continue

                try:
                    log("Iterating over readable sockets")
                    for sread in readable:
                        if sread is dest:
                            log("Bouncing from destination to client")
                            swrite = client
                        else:  # is client
                            log("Bouncing from client to destination")
                            swrite = dest

                        log("Receiving data")
                        data = sread.recv(buffer_size)

                        if not data:
                            raise QuitProxyLoop("No data to receive")

                        log("Sending data")
                        swrite.send(data)

                except socket.error:
                    log("Socket error in the loop", True)
                    raise QuitProxyLoop("Socket error")
                
        except QuitProxyLoop as e:
            log("Terminated the loop: " + e.args[0])
                
    except AbortedConnection as e:

        log("Connection aborted: " + e.args[0])

    finally:
        
        if dest:
            log("Closing the destination connection")
            dest.close()

        log("Closing the client connection")
        client.close()

    log("The thread is terminated")


def main():

    global thread_id
    
    err_code = 1  # default exit status
    
    # Redirecting SIGTERM into SIGINT
    signal.signal(signal.SIGTERM, receive_sigterm)

    # Reload configuration by a SIGHUP
    signal.signal(signal.SIGHUP, load_config)

    # Load configuration at start
    load_config()

    # Starting

    log("Cewarib v1.0 - Log enabled")
    log("Starting the SOCKSv5 proxy server")

    if socket.has_ipv6:
        log("Creating an IPv6 server socket")
        server = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

        if socket.has_dualstack_ipv6():
            log("Enabling IPv4 for the socket")
            server.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 0)
        else:
            log("Platform does not support dualstack.  Server is IPv6-only")
    else:

        log("Platform does not support IPv6.  Server is IPv4-only")
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:

        try:
            server.settimeout(socket_timeout)
            server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            log("Binding to all interfaces through port {}".format(server_port))
            server.bind(("", server_port))
            server.listen(max_connections)
            log("Now listening")

        except socket.error:
            log("Cannot start the server", True)
            raise ServerFailure("Cannot start the server")

        while True:

            if threading.active_count() > max_connections:
                log("Maximum number of connections has been reached")
                continue

            try:
                client, address = server.accept()
                client.setblocking(True)

                thread_id += 1
                log("Accepting connection #{} from {}".format(thread_id,
                    repr(address)))

                threading.Thread(target = undergo_client_connection,
                        args = (client, )).start()
         
            except KeyboardInterrupt:
                log("Interrupted by user")
                err_code = 0
                raise ServerFailure("Interrupted by user")

            except socket.timeout:
                log("Socket timeout")
                pass

            except socket.error:
                log("General socket error", True)

            except:
                log("Unknown fatal error", True)
                raise ServerFailure("Unknown fatal error")

    except ServerFailure as e:
        log("Server failed: " + e.args[0])

    finally:
        server.close()
        
        exit(err_code)



if __name__ == "__main__":
    
    pid_path = "/run{}cewarib.pid".format("/" if os.getuid() == 0 else
            "/user/{}/".format(os.getuid()))

    # Writing PID to the pid file
    if os.path.exists(pid_path):
        print("Another Cewarib process is probably running.  Exiting")
        exit(1)

    with open(pid_path, "w") as f:
        f.write(str(os.getpid()))
    
    try:
        main()
    except:
        # Deleting the pid file
        os.remove(pid_path)
        raise
